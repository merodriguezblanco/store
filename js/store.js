(function ($) {

  'use strict';

  var BaseView,
      SwappingRouter,
      Home = {},
      Items = {},
      Cart = {},
      ProductReviews = {},
      Store, Utils;

  Utils = {

    formatCurrency: function (value, symbol) {
      var sign = '',
          valueFloat = parseFloat(value),
          valueString;

      if (isNaN(valueFloat)) {
        return value;
      }

      if (valueFloat < 0) {
        sign = '-';
      }

      valueFloat = Math.abs(valueFloat);
      valueFloat = parseInt((valueFloat + 0.005) * 100, 10);
      valueFloat = valueFloat / 100;
      valueFloat = valueFloat.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
      valueString = valueFloat.toString();

      // if the string doesn't contains a .
      if (!~valueString.indexOf('.')) {
        valueString += '.00';
      }
      // if it only contains one number after the .
      else if (valueString.indexOf('.') === (valueString.length - 2)) {
        valueString += '0';
      }

      symbol = symbol || '$';

      return sign + symbol + valueString;
    }

  }

  _.mixin(Utils);

  BaseView = Backbone.View.extend({

    setMetaDescriptionTag: function () {
      var self = this;

      $('meta[name="description"]').attr({
        content: self.metaDescription || ''
      });
    },

    setMetaKeywordsTag: function () {
      var self = this;

      $('meta[name="keywords"]').attr({
        content: self.metaKeywords || ''
      });
    },

    setMetaTags: function () {
      var self = this;

      this.setMetaDescriptionTag();
      this.setMetaKeywordsTag();
    },

    setTitle: function () {
      document.title = this.title || '';
    },

    _render: function () {
      this.setTitle();
      this.setMetaTags();
    },

    leave: function () {
      this.off();
      this.remove();
    }

  });

  SwappingRouter = Backbone.Router.extend({

    swap: function (newView) {
      if (this.currentView && this.currentView.leave) {
        this.currentView.leave();
      }

      this.currentView = newView;
      this.currentView.render();
      $(this.el).empty().append(this.currentView.el);
    }

  });

  Items.Models = {};

  Items.Models.OrderItem = Backbone.Model.extend({

    price: function () {
      return this.get('product').get('price') * this.get('quantity');
    }

  });

  Items.Models.ListItem = Backbone.Model.extend({});

  Items.Models.Details = Backbone.Model.extend({

    model: Items.Models.ListItem,

    url: 'http://localhost/store/products/1.json',

    parse: function (response) {
      response.reviews = new ProductReviews.Collection(response.reviews);
      return response;
    }

  });

  Items.Collection = {};

  Items.Collection.OrderItems = Backbone.Collection.extend({

    model: Items.Models.OrderItem,

    findOrCreateOrderItem: function (orderItem) {
      var itemId = orderItem.get('product').get('id'),
          searchedItem = _.find(this.models, function (item) {
            return parseInt(item.get('product').get('id'), 10) === parseInt(itemId, 10);
          });

      if (searchedItem) {
        searchedItem.set({ quantity: orderItem.get('quantity') });
        this.trigger('change');
        return searchedItem;
      }

      this.add(orderItem);

      return orderItem;
    },

    getTotalCount: function () {
      return _.reduce(this.models, function (sum, item) {
        return item.get('quantity') + sum;
      }, 0);
    },

    getTotalCost: function () {
      return _.reduce(this.models, function (sum, item) {
        return item.price() + sum;
      }, 0);
    }

  });

  Items.Collection.Items = Backbone.Collection.extend({

    model: Items.Models.ListItem,

    url: 'http://localhost/store/js/items.json'

  });

  Items.Views = {};

  Items.Views.ListItem = BaseView.extend({

    tagName: 'li',

    template: _.template($('#products-list-item-template').html()),

    render: function () {
      this.$el.html(this.template(this.model.attributes));
      return this;
    }

  });

  Items.Views.List = BaseView.extend({

    template: _.template($('#products-list-template').html()),

    title: 'Products',

    metaKeywords: 'products, products list',

    metaDescription: '',

    initialize: function () {
      this.collection.on('reset', this.addAllItems, this);
    },

    addItem: function (item) {
      var view = new Items.Views.ListItem({
        model: item
      });
      this.$('.products-list').append(view.render().el);
    },

    addAllItems: function () {
      _.each(this.collection.models, this.addItem, this);
    },

    render: function () {
      this._render();
      this.$el.html(this.template);
      this.addAllItems();
      return this;
    }

  });

  Items.Views.Details = BaseView.extend({

    template: _.template($('#product-details-template').html()),

    events: {
      'change [name="quantity"]': 'updateQuantity',
      'keypress [name="quantity"]': 'ignoreEnter',
      'click [data-type="add-to-cart"]': 'addToCart'
    },

    initialize: function (options) {
      var self = this;

      this.order = options.order;
      this.orderItem = new Items.Models.OrderItem({
        product: self.model,
        quantity: 0
      });
      this.title = this.model.get('name');
      this.metaKeywords = this.model.get('meta_keywords');
      this.metaDescription = this.model.get('meta_description');
    },

    renderReviews: function () {
      var self = this,
          reviews = new ProductReviews.Views.ReviewsList({
            collection: self.model.get('reviews').getApprovedReviews(),
            el: self.$('#product-reviews')
          });

      this.$el.append(reviews.render().el);
    },

    render: function () {
      this._render();
      this.$el.html(this.template(this.model.attributes));
      this.renderReviews();
      return this;
    },

    addToCart: function (event) {
      event.preventDefault();
      event.stopPropagation();

      var quantity = parseInt(_.escape(this.$('[name="quantity"]').val()), 10);

      if (quantity > 0) {
        this.orderItem.set({ quantity: quantity });
        this.order.findOrCreateOrderItem(this.orderItem);
        Backbone.history.navigate('#cart', { trigger: true });
      }
    },

    ignoreEnter: function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        event.stopPropagation();
      }
    }

  });

  Home.View = BaseView.extend({

    template: _.template($('#home-template').html()),

    title: 'Welcome to Store',

    metaKeywords: 'home',

    metaDescription: '',

    setupHomeSlider: function () {
      this.$('#home-slider').owlCarousel({
        autoPlay: 3000,
        items: 1
      });
    },

    render: function () {
      this._render();
      this.$el.html(this.template);
      this.setupHomeSlider();
      return this;
    }

  });

  Cart.Views = {};

  Cart.Views.ShoppingCart = BaseView.extend({

    template: _.template($('#cart-template').html()),

    title: 'Shopping Cart',

    metaKeywords: 'shopping cart, cart',

    metaDescription: '',

    events: {
      'change [name="quantity"]': 'updateQuantity',
      'click [data-action="remove-item"]': 'removeItem'
    },

    initialize: function () {
      this.collection.on('add change remove', this.render, this);
    },

    render: function () {
      this._render();
      this.$el.html(this.template(this.order));
      return this;
    },

    updateQuantity: function (event) {
      event.preventDefault();
      event.stopPropagation();

      var $target = this.$(event.currentTarget),
          itemId = parseInt(_.escape($target.data('id')), 10),
          quantity = parseInt(_.escape($target.val()), 10),
          item;

      if (quantity > 0) {
        item = _.find(this.collection.models, function (item) {
          return parseInt(item.get('product').get('id'), 10) === itemId;
        });

        if (item) {
          item.set({ quantity: quantity });
        }
      }
    },

    removeItem: function (event) {
      event.preventDefault();
      event.stopPropagation();

      var $target = this.$(event.currentTarget),
          itemId = parseInt(_.escape($target.data('id')), 10),
          itemToRemove = _.find(this.collection.models, function (item) {
            return parseInt(item.get('product').get('id'), 10) === itemId;
          });

      if (itemToRemove) {
        this.collection.remove(itemToRemove);
      }
    }

  });

  Cart.Views.MiniCart = BaseView.extend({

    el: $('#mini-cart'),

    template: _.template($('#mini-cart-template').html()),

    initialize: function () {
      this.collection.on('add change remove', this.render, this);
    },

    render: function () {
      var self = this;

      this.$el.html(this.template({
        count: self.collection.getTotalCount(),
        cost: self.collection.getTotalCost()
      }));
    }

  });

  ProductReviews.Model = Backbone.Model.extend({

    urlRoot: 'http://localhost/store/products/1/reviews.json',

    default: {
      approved: false
    }

  });

  ProductReviews.Collection = Backbone.Collection.extend({

    url: 'http://localhost/store/products/1/reviews.json',

    model: ProductReviews.Model,

    getApprovedReviews: function () {
      return new ProductReviews.Collection(this.filter(function (review) {
        return review.get('approved') == "true";
      }));
    }

  });

  ProductReviews.Views = {};

  ProductReviews.Views.ReviewsList = Backbone.View.extend({

    template: _.template($('#product-reviews-template').html()),

    render: function () {
      this.$el.html(this.template());
      return this;
    }

  });

  Store = SwappingRouter.extend({

    routes: {
      '': 'home',
      'products': 'products',
      'products/:id': 'productDetails',
      'cart': 'cart'
    },

    initialize: function () {
      var self = this,
          miniCart;

      this.el = $('#container');
      this.order = new Items.Collection.OrderItems();
      miniCart = new Cart.Views.MiniCart({
        collection: self.order
      });
      miniCart.render();
    },

    home: function () {
      var view = new Home.View();

      this.swap(view);
      //view.render();
    },

    products: function () {
      this.items = new Items.Collection.Items();
      var self = this,
          itemsList = new Items.Views.List({
            collection: self.items
          });

      this.items.fetch({
        success: function () {
          self.swap(itemsList);
          //$('#container').html(itemsList.render().el);
        }
      });
    },

    productDetails: function (id) {
      var self = this,
          item = new Items.Models.Details(),
          itemView = new Items.Views.Details({
            model: item,
            order: self.order
          });

      // TODO: Remove this after having a DB.
      item.url = 'http://localhost/store/js/item' + id + '.json';
      item.fetch({
        data: {
          id: id
        },
        success: function () {
          self.swap(itemView);
        },
        failure: function () {
          console.log('ERROR: fetching product details.');
        }
      });
    },

    cart: function () {
      var self = this,
          cartView = new Cart.Views.ShoppingCart({
            collection: self.order
          });

      this.swap(cartView);
    }

  });

  $(function () {
    var store = new Store();
    Backbone.history.start();
  });

}(jQuery));
